const data = require('./DB/data');

const clients = data.clients;
const banks = data.banks;
const accounts = data.accounts;

/**
Ejercicio 0
@description Retornar un arreglo con los ID de los clientes
*/
function exercise0() {
    return clients.map(client => client.id);
}

/**
Ejercicio 1
@description Retornar un arreglo con los ID de los clientes ordenados por RUT
*/
function exercise1() {
    return [...clients].sort( (a, b) => b.rut - a.rut).map(client => client.id);
}

/**
Ejercicio 2
@description Retornar un arreglo con los nombres de los clientes, ordenados de mayor a menor
por la suma TOTAL de los saldos de las Cuentas
*/
function exercise2() {
    let new_set_clients = [...clients];
    let total_balance = 0;
    for (let index = 0; index < new_set_clients.length; index++) {
        total_balance = 0;
        for (let j = 0; j < accounts.length; j++) {
            if (accounts[j].clientId === new_set_clients[index].id) {
                total_balance += accounts[j].balance;
            }
        }
        new_set_clients[index] = Object.assign({}, new_set_clients[index], {total_balance: total_balance});
    }
    return new_set_clients.sort( (a, b) => b.total_balance - a.total_balance).map(client => client.name);
}

/**
Ejercicio 3
@description Devuelve un objeto cuyo índice es el nombre de los bancos
y cuyo valor es un arreglo de los ruts de los clientes ordenados alfabéticamente por 'nombre'
*/
function exercise3() {
    let banks_modified = new Object();
    let data = [];
    for (const bank of banks) {
        data = [];
        banks_modified[bank.name] = accounts.filter(account => account.bankId === bank.id);
        for (const client_bank of banks_modified[bank.name]) {
            let client = clients.find(client => client.id === client_bank.clientId);
            if (client && client.id) {
                // SE AGREGA VALIDACION PARA QUE NO REPITA LOS CLIENTES EN EL RESULTADO FINAL, YA QUE UN USUARIO TIENE VARIAS CUENTAS EN EL MISMO BANCO SEGUN LA DATA PROPORCIONADA
                // NO SE SOLICITA EN EL EJERCICIO PERO SE AGREGA PARA MEJORARLO, ESPERANDO QUE NO AFECTE EL RESULTADO ESPERADO.
                if (data.length > 0) {
                    let valid = data.find(data => data.id === client.id);
                    if (!valid) {
                        data.push(client);
                    }   
                }
                else {
                    data.push(client)
                }
            }
            
        }
        banks_modified[bank.name] = data.sort( (a, b) => {
            let val = a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase());
            return val === 0 && a !== b ? b.name.localeCompare(a) : val;
        })
        .map(client => client.rut);
    }

    return banks_modified;
}

/**
Ejercicio 4
@description Devuelve un arreglo ordenado de mayor a menor con el saldo de los clientes que
tengan más de 25000 en el banco 'SCOTIABANK'
*/
function exercise4() {
    let scotiabank = banks.find(bank => bank.name === 'SCOTIABANK');
    let accounts_aux = accounts.filter(account => account.bankId === scotiabank.id && account.balance > 25000);
    return accounts_aux.sort((a, b) => b.balance - a.balance).map(account => account.balance);
}

/**
Ejercicio 5
@description Devuelve un arreglo con la 'id' de los Bancos de menor a mayor por el
TOTAL de dinero que administran en las cuentas de sus clientes
*/
function exercise5() {
    let data = [];
    for (const bank of banks) {
        let total_balance = 0;
        for (const account of accounts) {
            if (account.bankId === bank.id) {
                total_balance += account.balance;
            }
        }
        data.push({idBank: bank.id, total_balance: total_balance});
    }
    return data.sort( (a, b) => a.total_balance - b.total_balance).map(bank => bank.idBank);
}

/**
Ejercicio 6
@description Devuelve un objeto en donde la key son los nombre de los bancos
y el valor es la cantidad de clientes que solo tienen una cuenta en ese banco
*/
function exercise6() {
    let banks_aux = new Object();
    let cant_clients = 0;
    let aux = [];
    for (const bank of banks) {
        cant_clients = 0;
        for (const client of clients) {
            aux = [];
            aux = accounts.filter(account => account.clientId === client.id && account.bankId === bank.id);
            if (aux.length === 1) {
                cant_clients += 1;
            }
        }
        banks_aux[bank.name] = cant_clients;
    }
    return banks_aux;
}

/**
Ejercicio 7
@description Devuelve un objeto en donde la key son el nombre de los bancos
y el valor es el 'id' de su cliente con menos dinero.
*/
function exercise7() {
    let data_aux = new Object;
    let aux = [];
    let menor = {};
    for (const bank of banks) {
        menor = {};
        aux = accounts.filter(account => account.bankId === bank.id);
        for (const account of aux) {
            if (menor && menor.id) {
                if (account.balance < menor.balance) {
                    menor.id = account.clientId;
                    menor.balance = account.balance;
                }
            }
            else {
                menor.id = account.clientId;
                menor.balance = account.balance;
            }
        }
        data_aux[bank.name] = menor.id;
    }
    return data_aux;
}

console.log("Ejercicio 0 --> ", exercise0() || "Ejercicio no resuelto");
console.log("Ejercicio 1 --> ", exercise1() || "Ejercicio no resuelto");
console.log("Ejercicio 2 --> ", exercise2() || "Ejercicio no resuelto");
console.log("Ejercicio 3 --> ", exercise3() || "Ejercicio no resuelto");
console.log("Ejercicio 4 --> ", exercise4() || "Ejercicio no resuelto");
console.log("Ejercicio 5 --> ", exercise5() || "Ejercicio no resuelto");
console.log("Ejercicio 6 --> ", exercise6() || "Ejercicio no resuelto");
console.log("Ejercicio 7 --> ", exercise7() || "Ejercicio no resuelto");

process.exit();